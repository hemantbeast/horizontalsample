﻿using System;
using System.Collections.Generic;
using System.Linq;
using FFImageLoading;
using FFImageLoading.Forms.Platform;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace HorizontalSample.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			Forms.Init();
			CachedImageRenderer.Init();
			Sharpnado.Presentation.Forms.iOS.SharpnadoInitializer.Initialize();

			var config = new FFImageLoading.Config.Configuration() {
				VerboseLogging = false,
				VerbosePerformanceLogging = false,
				VerboseMemoryCacheLogging = false,
				VerboseLoadingCancelledLogging = false,
			};
			ImageService.Instance.Initialize(config);

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}
