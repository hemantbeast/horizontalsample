﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using FFImageLoading;

namespace HorizontalSample.Droid
{
	[Activity(Label = "HorizontalSample", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(savedInstanceState);

			Forms.Init(this, savedInstanceState);
			Sharpnado.Presentation.Forms.Droid.SharpnadoInitializer.Initialize();
			FFImageLoading.Forms.Platform.CachedImageRenderer.Init(true);

			var config = new FFImageLoading.Config.Configuration() {
				VerboseLogging = false,
				VerbosePerformanceLogging = false,
				VerboseMemoryCacheLogging = false,
				VerboseLoadingCancelledLogging = false,
			};
			ImageService.Instance.Initialize(config);

			LoadApplication(new App());
		}
	}
}