﻿using System;
using System.Collections.ObjectModel;

namespace HorizontalSample
{
	public class MainPageViewModel : BaseViewModel
	{
		ObservableCollection<AnnouncementModel> _announcementist;

		public MainPageViewModel()
		{
			var desc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ut tellus lorem. Etiam sed pretium nibh. Cras sem mauris, mattis et feugiat nec, finibus ac nulla. Sed rhoncus orci nec aliquam congue. Sed placerat maximus fermentum. Vestibulum eleifend ut massa vel mattis. Aliquam varius risus id ultricies egestas. Aliquam at rutrum ligula." +
				Environment.NewLine + Environment.NewLine + "Ut nec velit sed eros eleifend posuere ut at turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis at enim tortor. Nam ac feugiat tortor. Maecenas a accumsan arcu. Integer hendrerit mi sit amet mi dapibus, eu mollis est gravida. Morbi mattis mollis nisl, eget hendrerit quam imperdiet ac. In imperdiet luctus pretium.";

			AnnouncementList = new ObservableCollection<AnnouncementModel> {
				new AnnouncementModel {
					Date = DateTime.Now.ToString("d'-'M'-'yyyy"),
					Image = "https://lorempixel.com/150/150",
					Title = "Title 1",
					Description = desc
				},
				new AnnouncementModel {
					Date = DateTime.Now.ToString("d'-'M'-'yyyy"),
					Image = "https://lorempixel.com/200/200",
					Title = "Title 2",
					Description = desc
				},
				new AnnouncementModel {
					Date = DateTime.Now.ToString("d'-'M'-'yyyy"),
					Image = "https://lorempixel.com/250/250",
					Title = "Title 3",
					Description = desc
				},
				new AnnouncementModel {
					Date = DateTime.Now.ToString("d'-'M'-'yyyy"),
					Image = "https://lorempixel.com/150/150",
					Title = "Title 4",
					Description = desc
				},
				new AnnouncementModel {
					Date = DateTime.Now.ToString("d'-'M'-'yyyy"),
					Image = "https://lorempixel.com/200/200",
					Title = "Title 5",
					Description = desc
				},
				new AnnouncementModel {
					Date = DateTime.Now.ToString("d'-'M'-'yyyy"),
					Image = "https://lorempixel.com/250/250",
					Title = "Title 6",
					Description = desc
				}
			};
		}

		public ObservableCollection<AnnouncementModel> AnnouncementList {
			get => _announcementist;
			set {
				_announcementist = value;
				OnPropertyChanged();
			}
		}
	}
}
