﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HorizontalSample
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
			Title = "Horizontal Listview";

			FlowDirection = FlowDirection.RightToLeft;
			//FlowDirection = FlowDirection.LeftToRight;

			BindingContext = new MainPageViewModel();
		}
	}
}
