﻿using System;

namespace HorizontalSample
{
	public class AnnouncementModel
	{
		public string Image { get; set; }
		public string Title { get; set; }
		public string Date { get; set; }
		public string Description { get; set; }
	}
}